import pathlib
from dataclasses import dataclass
import tarfile 
import yaml
import os

@dataclass
class PackageData :
    name : str
    archive_path : pathlib.Path
    out_path : pathlib.Path

    def unpack(self) :
        file = tarfile.open(os.path.abspath(self.archive_path)) 
        file.extractall(os.path.abspath(self.out_path)) 
        file.close()

def load_packages_info(packages_dir : pathlib.Path) -> list[PackageData]:
    out_dir = pathlib.Path(packages_dir, 'out')
    config_file = pathlib.Path(packages_dir, 'info.yml')
    packages_list = []
    with open(config_file, 'r') as file :
        config = yaml.safe_load(file)
        for packages in config :
            for name in packages :
                packages_list.append(PackageData(str(name), pathlib.Path(packages_dir, packages[name]["archive_path"]), pathlib.Path(out_dir, str(name))))
    
    return packages_list