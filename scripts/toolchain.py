archs = {
   "32bit",
   "64bit" 
}

platforms = {
    "Windows", 
    "Linux"
}

compilers = {
    "clang",
    "gcc",
    "msvc"
}



def cmake_toolchain_name(arch, target_platform, target_compiler) -> str :
    if arch == "32bit" and target_platform == "Windows"   and target_compiler == "gcc"      : return "x86-windows-gcc.cmake"
    if arch == "32bit" and target_platform == "Windows"   and target_compiler == "clang"    : return "x86-windows-clang.cmake"
    if arch == "32bit" and target_platform == "Linux"     and target_compiler == "gcc"      : return "x86-linux-gcc.cmake"
    if arch == "32bit" and target_platform == "Linux"     and target_compiler == "clang"    : return "x86-linux-clang.cmake"
    if arch == "64bit" and target_platform == "Windows"   and target_compiler == "gcc"      : return "x64-windows-gcc.cmake"
    if arch == "64bit" and target_platform == "Windows"   and target_compiler == "clang"    : return "x64-windows-clang.cmake"
    if arch == "64bit" and target_platform == "Linux"     and target_compiler == "gcc"      : return "x64-linux-gcc.cmake"
    if arch == "64bit" and target_platform == "Linux"     and target_compiler == "clang"    : return "x64-linux-clang.cmake"
    return None