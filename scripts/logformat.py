import logging

class LogFormat(logging.Formatter):
    prefix = {  logging.INFO    : "[inf] ", 
                logging.WARNING : "[war] ",
                logging.ERROR   : "[err] ",
                logging.DEBUG   : "[deb] "}
    
    suffix = {  logging.INFO    : "", 
                logging.WARNING : " ... ",
                logging.ERROR   : " ! ",
                logging.DEBUG   : ""}
        
    def __init__(self, *args, **kwargs):
        super(LogFormat, self).__init__(*args, **kwargs)

    def format(self, record):
        record.msg = self.prefix[record.levelno] + record.msg + self.suffix[record.levelno]
        return logging.Formatter.format(self, record)