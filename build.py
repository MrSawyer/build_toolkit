import sys
import argparse
import logging
import pathlib
import os
import platform
import shutil
import subprocess

import scripts.logformat    as logformat
import scripts.toolchain    as toolchain
import scripts.package      as package

logger = logging.getLogger("Build")
logger.setLevel(logging.INFO)

"""
Removes directory tree
"""
def clean(path : pathlib.Path):
    logger.debug(f"Removing directory {path}")
    shutil.rmtree(path, ignore_errors=True)

"""
Prepares directory
"""
def prepare(path : pathlib.Path) :
    logger.debug(f"Preparing directory {path}")
    if not pathlib.Path.exists(path) :
        logger.debug(f"Directory {path} does not exists.")
        logger.debug(f"Creating empty one.")
        os.makedirs(path)
    if not os.path.isdir(path) : 
        logger.error(f"Path {path} is not a valid directory")
        return False
    return True

"""
CMake project configure
"""
def configure(root : pathlib.Path, sources : pathlib.Path, output : pathlib.Path,  args : list[str]) :
    logger.debug(f"Configuring project from {sources} into {output}")
    if len(args) > 0 :
        logger.debug(f" additional configuration arguments {args}")
    os.chdir(output)
    ret = subprocess.call(["cmake", f"-S{str(sources)}", f"-B{str(output)}"] + args)
    os.chdir(root)
    return ret == 0

"""
Ninja project build
"""
def build(root : pathlib.Path, output : pathlib.Path, args : list[str]) :
    logger.debug(f"Building project from {root} into {output}")
    if len(args) > 0 :
        logger.debug(f" additional build arguments {args}")
    os.chdir(output)
    ret = subprocess.call(["ninja"] + args)
    os.chdir(root)
    return ret == 0

def main() -> int:
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    ch.setFormatter(logformat.LogFormat())
    logger.addHandler(ch)

    parser = argparse.ArgumentParser(
                prog='build',
                description='Build system',
                epilog='---')
    
    parser.add_argument("project", action="store", type=pathlib.Path, help = "specifiy project root directory")
    parser.add_argument("--src", action="store", default = pathlib.Path(""), type=pathlib.Path, help = "specifiy sources directory, containing root CMakeLists")
    parser.add_argument("--toolchains", action="store", default = pathlib.Path("cmake_toolchains"), type=pathlib.Path, help = "specifiy toolchains directory, containing toolchain.cmake files")
    parser.add_argument("--out", action="store", default = pathlib.Path("build"), type=pathlib.Path, help = "specifiy build output directory, default to `build/`")
    parser.add_argument("--debug", "-d", action="store_true", default = False, help = "build debug configuration, default `True`")
    parser.add_argument("--clean", "-c", action="store_true", help = "clean build directory")
    parser.add_argument("--verbose", "-v", action="store_true", help = "show additional information during building")
    parser.add_argument("--tests", action="store_true", default = False, help = "build tests, default `False`")
    parser.add_argument("--arch", choices=sorted(toolchain.archs), default = "", required=False, help = "chose target architecture")
    parser.add_argument("--platform", choices=sorted(toolchain.platforms), default = "", required=False, help = "chose target platform")
    parser.add_argument("--compiler", choices=sorted(toolchain.compilers), default = "", required=False, help = "chose target compiler")
    parser.add_argument('--version', action='version', version='%(prog)s 1.0')
    parser.add_argument("--target", "-t", action="store", default = "all", type=str, help = "set building target, default `all`")
    parser.add_argument("--packages", action="store", default = [], type=list[str], help = "specify set of packages to prepare")
    args = parser.parse_args()

    #detect defaults
    args.arch = args.arch if args.arch else platform.architecture()[0]
    args.platform = args.platform if args.platform else platform.system()
    args.compiler = args.compiler if args.compiler else "gcc"

    # subprocess arguments
    cmake_args = ["-GNinja", "-Wno-dev"]
    generator_args = ["-j30"]

    # structure
    root_dir = pathlib.Path(os.path.dirname(os.path.realpath(__file__)))
    toolchains_dir = pathlib.Path(root_dir, args.toolchains)
    packages_dir = pathlib.Path(root_dir, "packages")
    packages_list = package.load_packages_info(packages_dir)

    for pck in packages_list :
        logger.info(f"Preparing package {pck.name}")
        pck.unpack()

    toolchain_name = toolchain.cmake_toolchain_name(args.arch, args.platform, args.compiler)
    toolchain_path = pathlib.Path(toolchains_dir, toolchain_name)

    project_dir = pathlib.Path(os.path.abspath(args.project))
    src_dir = pathlib.Path(project_dir, args.src)
    out_dir = pathlib.Path(project_dir, args.out)

    logger.info(f"toolkit dir: {os.path.abspath(root_dir)}")
    logger.info(f"toolkit toolchains dir: {os.path.abspath(toolchains_dir)}")

    logger.info(f"Configure project '{os.path.basename(project_dir)}'")
    logger.info(f"Project dir: {os.path.abspath(project_dir)}")
    logger.info(f"Sources dir: {os.path.abspath(src_dir)}")
    logger.info(f"Build dir: {os.path.abspath(out_dir)}")

    logger.info(f"Target architecture: {args.arch}")
    logger.info(f"Target platform: {args.platform}")
    logger.info(f"Target compiler: {args.compiler}")

    if not (project_dir in out_dir.parents) : 
        logger.warning(f"Path {out_dir} is not in subtree of {project_dir}")

    if args.verbose :
        logger.setLevel(level=logging.DEBUG)
        logger.debug(f"Setting verbose mode")
        cmake_args.append("--loglevel=DEBUG")
        cmake_args.append("--debug-output")
        generator_args.append("-v")        
    
    if args.debug :
        logger.info(f"Building in debug mode")
        cmake_args.append("-DCMAKE_BUILD_TYPE=Debug")

    if args.tests :
        logger.info(f"Building with tests")
        cmake_args.append("-DBUILD_TESTING=True")
    else :
        logger.info(f"Building without tests")
        cmake_args.append("-DBUILD_TESTING=False")
    
    if toolchain_path.exists() :
        logger.info(f"Toolchain file: {os.path.abspath(toolchain_path)}")
        cmake_args.append(f"-DCMAKE_TOOLCHAIN_FILE={str(toolchain_path)}")
    else :
        logger.error(f"Toolchain file: {os.path.abspath(toolchain_path)} does not exist")

    if args.target :
        logger.info(f"Building target [ { args.target} ]")
        generator_args.append(str(args.target)) 
    
    if args.clean :
        logger.info("Clean build")
        clean(out_dir)

    if not prepare(out_dir) : return -1
    if not configure(project_dir, src_dir, out_dir, cmake_args) : return -1
    if not build(project_dir, out_dir, generator_args) : return -1
     
    return 0

if __name__ == '__main__':
    sys.exit(main())
